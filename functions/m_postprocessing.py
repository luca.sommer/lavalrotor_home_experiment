"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    x_acceleration_squared = np.square(x)
    y_acceleration_squared = np.square(y)
    z_acceleration_squared = np.square(z)
    
    absolut_acceleration = np.sqrt(x_acceleration_squared+y_acceleration_squared+z_acceleration_squared)
    
    return absolut_acceleration

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    time_interpolation = np.linspace(np.min(time), np.max(time), len(time))
    data_interpolated = np.interp(time_interpolation, time, data)
    return data_interpolated


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    mittelwert = np.mean(x)
    mittelwert_null = x-mittelwert
    
    x = mittelwert_null
    sr = len(time)/(np.max(time)-np.min(time))
    
    X = fft(x)
    N=len(X)
    n = np.arange(N)
    T = N/sr
    freq = n/T
    
    n_oneside = N//2
    f_oneside = freq[:n_oneside]
    
    X_oneside = X[:n_oneside]/n_oneside
    
    return X_oneside, f_oneside